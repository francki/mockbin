package com.newcomerproject.mockbin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockbinApplication {

	public static void main(String[] args) {
		SpringApplication.run(MockbinApplication.class, args);
	}

}
