package com.newcomerproject.mockbin.dto;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class RequestDTO {

    private Map<String, String> headers;
    private Map<String, String> params;
    private JSONObject body;
    private HttpServletRequest request;

    public RequestDTO(Map<String, String> headers, Map<String, String> params, JSONObject body,
                      HttpServletRequest request) {
        this.headers = headers;
        this.params = params;
        this.body = body;
        this.request = request;
    }

    public RequestDTO(Map<String, String> headers, Map<String, String> params, HttpServletRequest request) {
        this.headers = headers;
        this.params = params;
        this.request = request;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("method", request.getMethod());
        json.put("url", request.getRequestURI());
        JSONObject paramsJSON = new JSONObject();
        JSONObject headersJSON = new JSONObject();
        headers.forEach((key, value) -> headersJSON.put(key, value));
        json.put("headers", headersJSON);
        request.getParameterMap().forEach((key, value) -> paramsJSON.put(key, new JSONArray().add(value)));
        json.put("params", params);
        if (this.body != null) {
            json.put("body", body);
        }
        json.put("cookies", request.getCookies());
        return json;
    }
}
