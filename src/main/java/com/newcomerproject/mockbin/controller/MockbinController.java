package com.newcomerproject.mockbin.controller;

import com.newcomerproject.mockbin.service.MockbinService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
public class MockbinController {

    @Autowired
    private MockbinService mockbinService;

    @GetMapping(value = "/healthpoint")
    public HttpStatus getHealth() {
        return mockbinService.handleHealthpoint();
    }

    @GetMapping(value = "/request", produces = MediaType.APPLICATION_JSON_VALUE)
    public JSONObject getRequest(@RequestHeader(required = false) Map<String, String> headers,
                                 @RequestParam(required = false) Map<String, String> params,
                                 HttpServletRequest request) {
        return mockbinService.handleRequest(headers, params, request);
    }

    @PostMapping(value = "/test", produces = MediaType.APPLICATION_JSON_VALUE)
    public JSONObject postTest(@RequestHeader(required = false) Map<String, String> headers,
                           @RequestParam(required = false) Map<String, String> params,
                           @RequestBody(required = false) JSONObject body,
                           HttpServletRequest request) {
        return mockbinService.handleTest(headers, params, body, request);
    }
}