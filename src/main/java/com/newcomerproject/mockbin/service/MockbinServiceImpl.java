package com.newcomerproject.mockbin.service;

import com.newcomerproject.mockbin.dto.RequestDTO;
import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Service
public class MockbinServiceImpl implements MockbinService {


    @Override
    public HttpStatus handleHealthpoint() {
        return HttpStatus.OK;
    }

    @Override
    public JSONObject handleRequest(Map<String, String> headers, Map<String, String> params, HttpServletRequest request) {
        return new RequestDTO(headers, params, request).toJson();
    }

    @Override
    public JSONObject handleTest(Map<String, String> headers, Map<String, String> params, JSONObject body, HttpServletRequest request) {
        return new RequestDTO(headers, params, body, request).toJson();
    }

}

