FROM openjdk:11

# ARG JAR_FILE=target/*.jar

RUN mkdir -p /home/app

COPY target/*.jar /home/app/mockbin.jar

ENTRYPOINT ["java", "-jar", "/home/app/mockbin.jar"]

EXPOSE 8080

#run maven clean install dans l'ide avant de faire quoi que ce soit
#docker build -t franckfranck615/mockbin:0.0.1 .
#docker run -p 8080:8080 franckfranck615/mockbin:0.0.1