package com.newcomerproject.mockbin.service;

import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface MockbinService {

    /**
     * Returns status 200. Used to check communication health.
     *
     * @return HttpsStatus : an httpStatus with status '200 OK'
     */
    HttpStatus handleHealthpoint();

    /**
     * Returns all the information of a GET HttpRequest.
     *
     * @param headers : the headers of the incoming http request
     * @param params : the request parameters of the incoming http request
     * @param request : the incoming http request
     * @return a json String that sums-up all this information
     */
    JSONObject handleRequest(Map<String, String> headers, Map<String, String> params, HttpServletRequest request);

    /**
     *  Returns all the information of a POST HttpRequest, including its body if it has one.
     *
     * @param headers : the headers of the incoming http request
     * @param params : the request parameters of the incoming http request
     * @param requestBody : the body of the incoming http request
     * @param request : the incoming http request
     * @return a json String that sums-up all this information
     */
    JSONObject handleTest(Map<String, String> headers, Map<String, String> params, JSONObject requestBody, HttpServletRequest request);

}
