package com.newcomerproject.mockbin.controller;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class MockbinControllerTest {

    private HttpClient   client;
    private HttpRequest  request;
    private HttpResponse response;
    private final String URI_ROOT_PATH_AND_PORT = "http://localhost:8080";

    //@Test
    public void whenCallHealthpoint_thenStatus200() throws IOException, InterruptedException {
        client = HttpClient.newHttpClient();
        request = HttpRequest.newBuilder(
                URI.create(URI_ROOT_PATH_AND_PORT + "/healthpoint"))
                .GET()
                .build();
        response = client.send(request, HttpResponse.BodyHandlers.discarding());
        assertEquals(response.statusCode(), HttpStatus.OK.value());
    }

    //@Test
    public void whenCallingNonExistingURL_thenStatus404() throws IOException, InterruptedException {
        client = HttpClient.newHttpClient();
        request = HttpRequest.newBuilder(
                URI.create(URI_ROOT_PATH_AND_PORT + "/nonExistingURL"))
                .GET()
                .build();
        response = client.send(request, HttpResponse.BodyHandlers.discarding());
        assertEquals(response.statusCode(), HttpStatus.NOT_FOUND.value());
    }

    //@Test
    public void whenCallingRequest_thenStatus200WithHeaders() throws IOException, InterruptedException, ParseException {
        client = HttpClient.newHttpClient();
        request = HttpRequest.newBuilder(
                URI.create(URI_ROOT_PATH_AND_PORT + "/request"))
                .GET()
                .build();
        response = client.send(request, HttpResponse.BodyHandlers.ofString());
        // check Http status OK (value = 200)
        assertEquals(HttpStatus.OK.value(), response.statusCode());
        // check that a `headers` field exists in the response
        JSONParser parser = new JSONParser();
        JSONObject jsonResponseBody = (JSONObject) parser.parse(response.body().toString());
        String headers = jsonResponseBody.get("headers").toString();
        assert(headers != null && headers != "" && headers.length() > 0);
    }

    //@Test
    public void whenCallingTestEndpoint_thenReturnStatus200andJSONBody() throws IOException, InterruptedException, ParseException {
        // create a sample JSON body object of 2 fields
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("key1", "value1");
        jsonBody.put("key2", "value2");

        client = HttpClient.newHttpClient();
        request = HttpRequest.newBuilder(
                URI.create(URI_ROOT_PATH_AND_PORT + "/test"))
                .header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .POST(HttpRequest.BodyPublishers.ofString(jsonBody.toString()))
                .build();
        response = client.send(request, HttpResponse.BodyHandlers.ofString());
        // check Http status OK (value = 200)
        assertEquals(HttpStatus.OK.value(), response.statusCode());
        // check that the JSON body of the response is not null
        JSONParser parser = new JSONParser();
        JSONObject jsonResponseBody = (JSONObject) parser.parse(response.body().toString());
        String body = jsonResponseBody.get("body").toString();
        assert(body != null && body != "" && body.length() > 0);

    }
}
